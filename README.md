﻿﻿GRMR v0.0.4

GRMR is a work-in-progress library of assets for the production of formal grammar strings, which can then be parsed to create procedurally generated levels, NPCs etc.

INSTALLATION:

1. Download the `grmr.gmez` file.
2. In Gamemaker: Studio, import an extension. When the file picker dialogue box appears, locate `grmr.gmez` and import it.
3. Double-click the extension in the resource tree. In the import resources tab, import the scripts (note: `scr_test_grammar` and `scr_test_file_funcs` are optional demonstration files), objects and the included file (this document).

USING GRMR: 

1. An instance of `obj_grammar` is created using `grmr_create()`.
2. Production rules for the `obj_grammar` instance are set using `grmr_set_rule()`. These are represented as `obj_rule` instances. Their ids are stored in the grammar's `list_rules` datastructure. Symbols extracted from these rules are stored in the grammar's `list_terminals` and `list_nonterminals` datastructures.
3. Strings are produced using `grmr_produce()`. The string is stored as the instance variable `current_string` in an instance of `obj_utterance` (if the `bool_slow` is set to false) or `obj_utter_slow`(if the `bool_slow` is set to true). These can be destroyed (in the usual manner) whenever the user no longer requires them.
4. The grammar, its rules and associated datastructures remain in memory until destroyed with `grmr_destroy()`.

At the present time, `obj_grammar`, `obj_rule`, `obj_utterance` and `obj_utter_slow` instances should only be created through the manner described above (ie. not through the room editor or use of `instance_create()`).
The sample script `scr_test_grammar()` demonstrates this procedure by creating a "street grammar" which populates a number of houses along a street. The strings produced by this grammar are printed in the compile window.

KEY SCRIPTS:

`grmr_create(axiom,label_string)` creates an instance of `obj_grammar` with the axiom/starting string (a string) and name/label (a string) you specify. See Symbol Guidelines for advice on specifying the starting string.

`grmr_set_rule(leftside,rightside, [weight])` creates an instance of `obj_rule` with a lefthand and righthand component you specify. These rules are specific to the grammar they are created for. Both the left and righthand component should be expressed as strings; see Symbol Guidelines for advice on specifying symbols. The optional argument `weight` can be a real number greater than 0.0001; if no value is passed for this argument then the weight will default to 1.0000.

`grmr_produce(boolean_slow,grammar,max_iters, [bool_weight])` produces a string using the specified grammar over no more than the specified number of iterations. The string is stored in a new instance of `obj_utterance` (if `boolean_slow == false`) or `obj_utter_slow` (if `boolean_slow == true`), in the instance's `current_string` variable. The function returns the id of this instance. If `boolean_slow == false` the grammatical string will be complete at the time the script returns the instance ID. If `boolean_slow == true` the string the string will be produced over several frames, and will be complete when the instance's `working` variable is set to 'false'. See Fast VS Slow for advice on choosing between the two modes of production. If you do not wish production to stop after a number of iterations, set `max_iters` to -1. The optional argument `bool_weight` indicates whether production rules selection should favour rules with greater weight or not (defaults to false).

`grmr_destroy(grammar)` safely destroys the selected `obj_grammar` instance and `obj_rule` instances associated with it, along with their datastructures.

UTILITY SCRIPTS

`grmr_deformat(str,bool_underscores,bool_pipes,bool_epsilon)` returns a copy of the string `str` devoid of underscores (if `bool_underscores == true`), pipes (if `bool_pipes == true`) and the empty string indicator <> (if `bool_epsilon == true`). `str` can be an actual string or the instance ID of an obj_utterance or obj_utter_slow. This is useful for improving the human readability of grmr strings, but note that the string returned by `grmr_deformat` may be unsuitable for further grmr operations which rely on _ and | to demarcate symbols.

`grmr_pad_symbols(str)` returns a copy of the string `str` with whitespace inserted between each symbol. `str` can be an actual string or the instance ID of an obj_utterance or obj_utter_slow. This is useful for improving the human readability of grmr strings.

`grmr_write_file(grammar)` packages a grammar instance and its rule instances into an ini-formatted .grmr file and writes this file to the working directory. The file will be named <label>.grmr where <label> is the instance's `label` variable. The full file location is returned as a string.

`grmr_read_file(filename)` creates new grammar and rule instances from a file (produced by `grmr_write_file()`). The `filename` argument should be a string of a .grmr file's location. The id of the new grammar instance is returned.

`grmr_read_safe(filename)` creates new grammar and rule instances from a file (produced by `grmr_write_file()`). Unlike `grmr_read_file()` this method reconstructs the rule and symbol datastructures from scratch, which is important if the .grmr file was edited after being created. The `filename` argument should be a string of a .grmr file's location. The id of the new grammar instance is returned.

`grmr_string_to_array(str, [bool_deformat], [bool_spaces])` returns a 1d array containing the string `str`'s symbols in order, one symbol per cell. If the optional argument `bool_deformat == true` the symbols will be stripped of pipes, underscores and empty string markers. If the optional argument `bool_spaces == true` the symbols will be separated by spaces.

`grmr_array_to_string(arr, [bool_deformat], [bool_spaces])` returns a string containing the 1d array `arr`'s symbols in order. If the optional argument `bool_deformat == true` the symbols will be stripped of pipes, underscores and empty string markers.If the optional argument `bool_spaces == true` the symbols will be separated by spaces.

`grmr_print_list(bool_terminals, bool_nonterminals,bool_rules)` prints each of the current grammar's symbols of the appropriate type and/or rules to the compile window. 

`grmr_print_switch(bool_terminals, bool_nonterminals)` prints a switch statement to the compile window with each of the selected type of symbols as a case. If both types of symbol are selected, a separate switch statement will be generated for each.

`grmr_digest(utterance)` returns the `current_string` variable of the selected instance of obj_utterance or obj_utter_slow and destroys the instance, unless the instance has not finished production. In the latter case, the instance will NOT be destroyed and a warning message will be returned instead of `current_string`.

SYMBOL GUIDELINES:

For the purposes of this library, a symbol consists of one or or more string characters occuring in a particular order. Symbols are either terminal (not able to be replaced by any rule) or nonterminal (able to be replaced in a later iteration). Nonterminal symbols are identified using string_pos(), so it is important that they can be correctly located eg. without proper formatting the symbol `cat` could be incorrectly identified as part of other symbols `catch` `category` etc. The following guidelines are strongly suggested to ensure correct operation:

1. a symbol must be at least 1 character long, not including pipes or underscores. 
2. a terminal symbol must start and end `_` (underscore)
3. a non-terminal symbol MUST start and end with `|` (pipe)
4. stack operations should be represented with `[` (to push) and `]` (to pop), enclosed in either underscores or pipes as appropriate
5. an empty string is represented with `<>`, enclosed in either underscores or pipes as appropriate
6. except for the purposes of rules 2-4 a symbol MUST NOT contain the following characters `| _ [ ] # ' "`
7. whitespace enclosed in pipes or underscores will be preserved. un-enclosed whitespace may be removed at various points in the procedure

FAST VS SLOW:

Both fast and slow production of strings will apply production rules in the same way. However, the fast method (and `obj_utterance`) will always generate the string during the frame that they are called, whereas the slow method (and `obj_string_slow`) applies *one production rule per step event* until an end condition is reached. The `obj_utter_slow` instance variable `working` will be `true` until an end condition has been reached, at which point it will be switched to `false`. Fast and slow production can be used interchangably within the same project (even within the same grammar instance). 

Choosing the right mode of production depends on the complexity of the grammar you want to use. As a rule, we at Softer Plastics prefer having the completed string in as short a time as possible after we call `grmr_produce()`; we imagine many developers will feel the same way. However, fast production of strings will "block" the program from executing further code, which could be problematic when producing strings from highly-complex grammars. If it is more convenient to handle strings asyncronously than to have your program "hang" in these circumstances, then we recommend slow strings. Similarly, if you want to *start* generating many strings at the same time but don't necessarily need them to be ready at the same time, the slow setting allows you to produce the strings in parallel.

If in doubt, try fast first.

WEIGHTS

The probability of a rule being selected is equal to its weight divided by the sum of weights of all selectable rules. Thus, a higher-weighted rule will be chosen more often than lower-weighted rules, and a rule that is many times weightier than others will be selected at almost every opportunity.

Weights must be greater than 0.0001. If no weight is specified when grmr_set_rule() is called, the default value of 1 will be used.

CHANGELOG

0.0.0.4

+ `obj_string` and `obj_slow_string` have been renamed `obj_utterance` and `obj_utter_slow`, respectively, for greater clarity between an instance produced by grmr and a string value.
+ production rules can be given real number weights to change the probability of being selected during production, with greater weights increasing likelihood of selection. weights are set using an optional argument when calling `grmr_set_rule()`.
+ `grmr_set_rule()` returns the id of the new rule.
+ strings are produced with or without weighting by using an optional argument when calling `grmr_produce()`.
+ `grmr_pad_symbols()` is a utility script which inserts whitespace between the symbols of a string for improved readability.
+ `obj_grammar` instances now have a datastructure which contains all unique strings produced. the uniqueness of a string can be checked with the new utility script `grmr_test_uniqueness()`
+ `grmr_pad_symbols()` and `grmr_deformat()` now accept a string OR the id of an utterance, thanks to a new internal script `grmr_argument_type()`.
+ scripts now contain documentation in the form of comments.

0.0.0.3 

+ stricter demarcation of symbols within strings (terminals within _s, nonterminals within |s)
+ symbol lists are autopopulated by `grmr_set_rule()`
+ strings can now be produced by `grmr_produce()` with an argument indicating which production mode to use.
+ utility scripts for listing symbols and rules
+ utility scripts for packaging grammars and their rules as .grmr files
+ utility scripts for convertings strings to arrays and back.
+ grmr_deformat removes various grmr-related punctuation
+ grmr_digest destroys a completed string and returns its `current_string`
+ bugfixes and documentation updates


0.0.0.2

+ strings produced by `obj_string` or `obj_slow_string` instead of `obj_grammar`.
+ fast and slow string production.

0.0.0.1

+ first release of GRMR.

COMING SOON

It is intended that future releases of GRMR will include:

+ hierarchical rule selection. allowing the user to designate different "registers" of production rules, which can be switched between to dis/allow groups of rules at different points in production.
+ different modes of rule application. currently a selected rule is applied once per iteration, at the earliest point in the string that it possible to do so.
+ auto-generated API documentation.
+ example projects

SUGGESTIONS, QUESTIONS AND BUG REPORTS:

email: softerplastics@mail.com

twitter: @lakemistake

bitbucket: https://bitbucket.org/softerplastics/grmr
