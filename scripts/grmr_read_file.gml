/// grmr_write_file(filepath)
var filepath = argument0;

// argument 0 (filepath) -- string indicating the location of the .grmr file to be imported.
// imports a grammar from a .grmr file, creates obj_grammar and obj_rule instance/s and returns the ID of the obj_grammar instance.

var i;
var list_blank = ds_list_write(ds_list_create());
var vargrammar = instance_create(0,0, obj_grammar);   
ini_open(filepath)
(vargrammar).label = ini_read_string("head", "label", "default");
(vargrammar).starting_string = ini_read_string("head", "starting_string", "|S|");
(vargrammar).list_terminals = ds_list_create(); 
ds_list_read((vargrammar).list_terminals,ini_read_string("head", "list_terminals", list_blank));
(vargrammar).list_nonterminals = ds_list_create();
ds_list_read((vargrammar).list_nonterminals,ini_read_string("head", "list_nonterminals", list_blank));
var num_rules = ini_read_real("head","num_rules", 0);
(vargrammar).list_rules = ds_list_create();
var list = (vargrammar).list_rules;
(vargrammar).prior_prob = ds_priority_create();
for(i=1;i<(num_rules + 1);i+=1)
{
    var rule = instance_create(0,0,obj_rule);
    list[|i] = rule;
    with(rule)
    {
        grammar = vargrammar;
        number = i;
        leftside = ini_read_string(string(i), "leftside", "default");
        rightside = ini_read_string(string(i), "rightside", "default");
        prob_weight = ini_read_string(string(i), "prob_weight", 1);
        rule_string = leftside + " => " + rightside;
    }
}
ini_close();

return (vargrammar);
