/// grmr_destroy(grammar)
var grammar = argument0;

// argument 0 (grammar) -- the instance id of the grammar to destroy.
// destroys the grammar, its rules and their datastructures.

with(grammar)
{
    ds_priority_destroy(prior_prob);
    ds_list_destroy(list_terminals);
    ds_list_destroy(list_nonterminals);
    ds_list_destroy(list_uniques);
    for(i = 0; i < ds_list_size(list_rules); i += 1)
    {
        var rule = list_rules[|i];
        instance_destroy(rule);
    };
    ds_list_destroy(list_rules);
    instance_destroy(grammar)
}
