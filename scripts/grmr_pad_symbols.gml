/// grmr_pad_symbols(str)
var str = argument0;

// argument 0 (str) -- EITHER a string to be pad OR the id of an obj_utterance or obj_utter_slow instance. if an instance id is supplied, the string contained in its current_string variable is padded.
// returns a copy of the string with whitespace inserted between symbols

var argtype = grmr_argument_type(str);

if (argtype == "stringinstance")
{
    str = (str).current_string;
}
else if ((argtype == "other") || (argtype == "otherinstance"))
{
    show_debug_message("grmr_pad_symbols() was passed a value that was neither a string or an utterance instance!");
    return noone;
}

do
{
    if (string_pos("__", str) > 0)
    {
        str = string_replace(str, "__", "_ _");
    }
}
until(string_pos("__", str) == 0)

do
{
    if (string_pos("_|", str) > 0)
    {
        str = string_replace(str, "_|", "_ |");
    }
}
until(string_pos("_|", str) == 0)

do
{
    if (string_pos("|_", str) > 0)
    {
        str = string_replace(str, "|_", "| _");
    }
}
until(string_pos("|_", str) == 0)

do
{
    if (string_pos("||", str) > 0)
    {
        str = string_replace(str, "||", "| |");
    }
}
until(string_pos("||", str) == 0)

return str;

