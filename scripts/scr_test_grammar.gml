var gram = grmr_create("|S|","Street");
with(gram)
{
    grmr_set_rule("|S|", "|h| |h| |S|");
    grmr_set_rule("|S|", "|h| |h| |s|");
    grmr_set_rule("|s|", "|h| |h| |s|");
    grmr_set_rule("|s|", "|h| |s|");
    grmr_set_rule("|s|", "|h|");
    grmr_set_rule("|h|", "_house__[_|o|_]_");
    grmr_set_rule("|h|", "_house__[_|e|_]_");
    grmr_set_rule("|e|", "_under construction_");
    grmr_set_rule("|e|", "_for sale_");
    grmr_set_rule("|e|", "_foreclosed_");
    grmr_set_rule("|e|", "_haunted_");
    grmr_set_rule("|o|", "|a| |o|");
    grmr_set_rule("|o|", "|a| |p|");
    grmr_set_rule("|o|", "|a| |c|");
    grmr_set_rule("|o|", "|a| ");
    grmr_set_rule("|a|", "_adult_");
    grmr_set_rule("|c|", "_child_");
    grmr_set_rule("|c|", "_toddler_");
    grmr_set_rule("|c|", "_teenager_");
    grmr_set_rule("|p|", "_dog_");
    grmr_set_rule("|p|", "_cat_");
    grmr_set_rule("|p|", "_bird_");
};
global.string_fast = grmr_produce(false, gram, 99);
//global.string_slow = grmr_produce(true, gram, 99);
global.array_fast = grmr_string_to_array((global.string_fast).current_string);
global.string_new = grmr_array_to_string(global.array_fast);
show_debug_message(global.string_new);

with(gram)
{
    grmr_print_list(true,true,true);
    grmr_print_switch(true,true);
};




