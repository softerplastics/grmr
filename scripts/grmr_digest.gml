/// grmr_digest(utterance)
var utterance = argument0;

// argument 0 (utterance) -- id of the string to digest.
// checks that a obj_utterance or obj_utter_slow instance is finished working. if it is, the instance is destroyed and its current_string is returned. if not, returns an error msg.

var str = (utterance).current_string;
if (!(utterance).working)
{
    instance_destroy(utterance);
}
else
{
    str = "WARNING! TRIED TO DIGEST A WORKING STRING";
};

return str;
