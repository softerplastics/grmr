/// grmr_produce_string_fast(grammar, max_iters, bool_weighted)
var grammar = argument0, max_iters = argument1, bool_weighted = argument2;

//argument 0 (grammar) -- instance ID of the grammar with which the string will be produced
//argument 1 (max_iters) -- real number of the maximum number of iterations over which the string can be built. set to -1 to run indefinitely.
//argument 2 (bool_weighted) -- boolean indicating whether to use weighted rule selection.
//produces a string contained in an instance of obj_utterance, using fast production (ie. production is complete by the time that the function is returned). Returns the id of the obj_utterance instance.

var str = instance_create(0,0,obj_utterance);
with (str)
{
    from = grammar;
    current_string += (grammar).starting_string;
    history_string += (grammar).starting_string;
    max_iterations = max_iters;
    while (working)
    {
        iterations += 1;
        if ((grmr_contains_nonterminals(from)) && ((iterations < max_iterations) || (max_iterations == -1)))
        {
            var rule = grmr_choose_rule(from,bool_weighted);
            var output_string = grmr_apply_rule(rule);
            current_string = output_string;
            history_string += ("#" + output_string);
        }
        else
        {
            working = false;
            show_debug_message("String finished. Final string:");
            show_debug_message(current_string);
        };
    };
};

return str;
