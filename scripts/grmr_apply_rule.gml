/// grmr_apply_rule(rule)
var rule = argument0;

// argument 0 (rule) -- instance id of the rule to be applied
// makes a string by copying the current_string variable of the calling instance and applies a production rule to it. returns this string.

with (rule)
{
    var left = leftside;
    var right = rightside;
};
var work_string = string_replace(current_string, left, right);
return work_string;
