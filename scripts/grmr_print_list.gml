///grmr_print_list(bool_terminals, bool_nonterminals, bool_rules)
var bool_terminals = argument0, bool_nonterminals = argument1, bool_rules = argument2;

// argument 0 (bool_terminals) -- boolean indicating whether terminal symbols should be printed or not
// argument 1 (bool_nonterminals) -- boolean indicating whether nonterminal symbols should be printed or not
// argument 2 (bool_rules) -- -- boolean indicating whether production rules should be printed or not
// prints lists of the terminals, nonterminals and production rules to the compile window.

if(bool_terminals)
{
    show_debug_message(label + " terminals");
    for(i=0;i<ds_list_size(list_terminals);i+=1)
    {
        var symb = list_terminals[|i];
        show_debug_message(symb);
    }
    show_debug_message("");
}

if(bool_nonterminals)
{
    show_debug_message(label + " nonterminals");
    for(i=0;i<ds_list_size(list_nonterminals);i+=1)
    {
        var symb = list_nonterminals[|i];
        show_debug_message(symb);
    }
    show_debug_message("");
}

if(bool_rules)
{
    show_debug_message(label + " rules");
    for(i=0;i<ds_list_size(list_rules);i+=1)
    {
        var rule = list_rules[|i];
        show_debug_message((rule).rule_string);
    }
    show_debug_message("");
}

