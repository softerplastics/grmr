/// grmr_test_uniqueness(utterance)
var utterance = argument0;
//argument 0 (utterance) -- instance ID of a obj_utterance or obj_utter_slow.
//searches for the instance's current_string in its grammar's list_uniques. if it cannot be found, adds current_string to the list and returns true, otherwise returns false
with (utterance)
{
    if(ds_list_find_index((grammar).list_uniques, current_string) == -1)
    {
        ds_list_add((grammar).list_uniques, current_string); //current_string is unique, add it to list
        return true;
    }
    else
    {
        return false;
    };
};
