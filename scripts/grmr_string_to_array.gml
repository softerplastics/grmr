///grmr_string_to_array(str, bool_deformat = false)
var str = argument[0];
var bool_deformat; if (argument_count > 1) bool_deformat = argument[1]; else bool_deformat = false;

// argument 0 (str) -- a string containing symbols to be converted into an array.
// optional argument 1 (bool_deformat) -- boolean indicating whether the string should be deformatted after being inserted into the array. defaults to false if no argument is passed
// converts a string containing grmr symbols into a 1d array of cells containing one symbol (as a string) each. returns the array.

var list = grmr_pull_symbol(str);
var array = array_create(1);
for(i=0;i<ds_list_size(list);i+=1)
{
    var symb = list[|i];
    if(bool_deformat)
    {
        symb = grmr_deformat(symb,true,true,true);
    }
    array[i] = symb;
};
ds_list_destroy(list);
return array;
