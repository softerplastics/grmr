///grmr_produce(bool_slow, grammar, max_iters, bool_weight = false)
var bool_slow = argument[0], grammar = argument[1], max_iters = argument[2];
var bool_weight; if (argument_count > 3) bool_weight = argument[3]; else bool_weight = false;

// argument 0 (bool_slow) -- boolean indicating whether or not the string production speed should be slow.
// argument 1 (grammar) -- instance ID of the grammar from which to produce a string.
// argument 2 (max_iters) -- real number of the maximum number of iterations over which the string can be built. set to -1 to run indefinitely.
// optional argument 3 (bool_weight) -- boolean indicating whether rules should be chosen according to their weights. defaults to false if no argument is passed.
// produces an instance of obj_utterance or obj_utter_slow containing an grammatical string and returns the instances ID.


if (bool_slow)
{
    var str = grmr_produce_string_slow(grammar,max_iters,bool_weight);
}
else
{
    var str = grmr_produce_string_fast(grammar,max_iters,bool_weight);
};

return str;
