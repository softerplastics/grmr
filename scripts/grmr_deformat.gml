/// grmr_deformat(str, bool_underscore, bool_pipe, bool_epsilon)
var str = argument0, bool_underscore = argument1, bool_pipe = argument2, bool_epsilon = argument3;

// argument 0 (str) -- EITHER a string to be deformatted OR the id of a obj_utterance or obj_utter_slow instance. if an instance id is supplied, the string contained in its current_string variable is deformatted.
// argument 1 (bool_underscore) -- boolean indicating whether underscores should be removed.
// argument 2 (bool_pipe) -- boolean indicating whether pipes should be removed.
// argument 3 (bool_epsilon) -- boolean indicating whether empty string indicators should be removed.
// removes grmr-related formatting from a string, and returns a copy of the deformatted string.

var argtype = grmr_argument_type(str);

if (argtype == "stringinstance")
{
    str = (str).current_string;
}
else if ((argtype == "other") || (argtype == "otherinstance"))
{
    show_debug_message("grmr_deformat() was passed a value that was neither a string or an obj_utterance instance!");
    return noone;
}

var i = 0;
for(i=1;i<(1+string_length(str));i+=1)
{
    var char = string_char_at(str, i);
    if ((char == "_") && (bool_underscore))
    {
        str = string_delete(str,i,1);
        i-=1;
    }
    else if ((char == "|") && (bool_pipe))
    {
        str = string_delete(str,i,1);
        i-=1;
    }
    else if ((char == "<") && (bool_epsilon))
    {
        str = string_delete(str,i,2);
        i-=2;
    }
}

return str;
