///grmr_choose_rule(grammar, bool_weighted)
var grammar = argument0, bool_weighted = argument1;

// argument 0 (grammar) -- instance ID of the grammar in which candidate rules are defined
// argument 1 (bool_weighted) -- boolean indicating whether to use weighted rule selection.
// scans the current_string variable of the calling instance for symbols matching the left side of each production rule in the chosen grammar. if a rule's left side matches part of current_string, the rule's ID is added to a list. returns a random item from this list, or noone if no rules match any part of current_string.
//TODO: probability weighting (next version)
//TODO: hierarchical selection

var i = 0;
var size = ds_list_size((grammar).list_rules);
if (bool_weighted)
{
    var candidates = (grammar).prior_prob;
    ds_priority_clear(candidates);
    ds_priority_add(candidates, "placeholder", 0); // we need a starting place for our numberline
    for(i = 0; i < size; i += 1)
    {
        var rule = (grammar).list_rules[|i];
        var left = (rule).leftside;
        if (string_pos(left,current_string) > 0)
        {
            var highest = ds_priority_find_max(candidates);
            highest = ds_priority_find_priority(candidates, highest);
            ds_priority_add(candidates, rule, (rule).prob_weight + highest); //add new point on the right of our numberline
        };
    };
    i = ds_priority_find_priority(candidates, ds_priority_find_max(candidates));
    if (i > 0)
    {
        i = random(i); //our random value
        rule = noone;
        do
        {
            var lowest = ds_priority_find_min(candidates); //find the leftmost point on the numberline.
            lowest = ds_priority_find_priority(candidates,lowest);
            rule = ds_priority_delete_min(candidates); //delete this point
            if(lowest <= i) //if our random value is greater than the current point on the numberline,
            {
                rule = noone; // keep moving right
            };
        }
        until (rule != noone);
        return rule;
    }
    else
    {
        return noone;
    };
}
else
{
    var candidates = ds_list_create();
    for(i = 0; i < size; i += 1)
    {
        var rule = (grammar).list_rules[|i];
        var left = (rule).leftside;
        if (string_pos(left,current_string) > 0)
        {
            ds_list_add(candidates, rule);
        };
    };
    i = ds_list_size(candidates) - 1;
    if (i > -1)
    {
        i = irandom(i);
        rule = candidates[|i];
        ds_list_destroy(candidates);
        return rule;
    }
    else
    {
        ds_list_destroy(candidates);
        return noone;
    };
}
