/// grmr_set_rule(left, right, weight = 1)
var left = argument[0], right = argument[1];
var weight; if (argument_count > 2) weight = argument[2]; else weight = 1;

// argument 0 (left) -- a string containing the symbols on the lefthand side of the production rule i.e. the symbols that will be replaced. these should include at least one nonterminal symbol.
// argument 1 (right) -- a string containing the symbols on the righthand side of the production rule i.e. the symbols that will replace the ones on the left.
// optional argument 2 (weight) -- real number indicating the weight of the rule when during selection. should be > 0. greater weight increases the chance of selection. defaults to 1 if no argument is passed.
// sets a production rule for the grammar which called this script.
//TODO: hierarchical selection

var rule = instance_create(0,0,obj_rule);
with (rule)
{
    leftside = left;
    rightside = right;
    grammar = other;
    var list_pulled = grmr_pull_symbol(left);
    for(i=0;i<ds_list_size(list_pulled);i+=1)
    {
        if(string_char_at(list_pulled[|i],1) == "_")
        {
            if(ds_list_find_index((grammar).list_terminals,list_pulled[|i]) == -1)
            {
                ds_list_add((grammar).list_terminals,list_pulled[|i]);
            };
        }
        else if(string_char_at(list_pulled[|i],1) == "|")
        {
            if(ds_list_find_index((grammar).list_nonterminals,list_pulled[|i]) == -1)
            {
                ds_list_add((grammar).list_nonterminals,list_pulled[|i]);
            };
        };
    };
    ds_list_destroy(list_pulled);
    var list_pulled = grmr_pull_symbol(right);
    for(i=0;i<ds_list_size(list_pulled);i+=1)
    {
        if(string_char_at(list_pulled[|i],1) == "_")
        {
            if(ds_list_find_index((grammar).list_terminals,list_pulled[|i]) == -1)
            {
                ds_list_add((grammar).list_terminals,list_pulled[|i]);
            };
        }
        else if(string_char_at(list_pulled[|i],1) == "|")
        {
            if(ds_list_find_index((grammar).list_nonterminals,list_pulled[|i]) == -1)
            {
                ds_list_add((grammar).list_nonterminals,list_pulled[|i]);
            };
        };
    };
    ds_list_destroy(list_pulled);
    ds_list_add((grammar).list_rules,id);
    number = ds_list_size((grammar).list_rules);
    rule_string = left + " => " + right;
    prob_weight = max(weight,0.00001);
};

return rule;
