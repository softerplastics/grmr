///grmr_print_switch(bool_terminals, bool_nonterminals)
var bool_terminals = argument0, bool_nonterminals = argument1;

// argument 0 (bool_terminals) -- boolean indicating whether terminal symbols should be printed or not
// argument 1 (bool_nonterminals) -- boolean indicating whether nonterminal symbols should be printed or not
// prints lists of the terminals and nonterminals to the compile window formatted as switch statements.

if(bool_terminals)
{
    show_debug_message("switch(symbol)")
    show_debug_message("{");
    for(i=0;i<ds_list_size(list_terminals);i+=1)
    {
        var symb = list_terminals[|i];
        show_debug_message("    case '" + symb + "':");
        show_debug_message("        //some code");
        show_debug_message("        break;");
    }
    show_debug_message("    default:");
    show_debug_message("}");
}

if(bool_nonterminals)
{
    show_debug_message("switch(symbol)")
    show_debug_message("{");
    for(i=0;i<ds_list_size(list_nonterminals);i+=1)
    {
        var symb = list_nonterminals[|i];
        show_debug_message("    case '" + symb + "':");
        show_debug_message("        //some code");
        show_debug_message("        break;");
    }
    show_debug_message("    default:");
    show_debug_message("}");
}

