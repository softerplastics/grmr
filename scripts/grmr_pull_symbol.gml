/// grmr_pull_symbol(str)
var str = argument0;

// argument 0 (str) -- symbol-containing string to be 'pulled apart'
// breaks up a string into its symbols and returns those symbols as strings in a ds_list. Non-symbolic content (ie phrases not enclosed in | or _) is ignored.

var result = "";
var list_result = ds_list_create();
var i = 0;
var symbol_started = false;
var symbol_seek = noone;

for(i=1;i<(1+string_length(str));i+=1)
{
    var char = string_char_at(str, i);
    if(!symbol_started)
    {
        if ((char == "_")||(char == "|"))
        {
            symbol_seek = char;
            symbol_started = true;
            result += char;
        }
    }
    else
    {
        result += char;
        if(char == symbol_seek)
        {
            ds_list_add(list_result,result);
            var symbol_started = false;
            var symbol_seek = noone;
            var result = "";
        }
    }
}

return list_result
