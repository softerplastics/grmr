///grmr_array_to_string(arr, bool_deformat = false, bool_spaces = false)
var arr = argument[0];
var bool_deformat; if (argument_count > 1) bool_deformat = argument[1]; else bool_deformat = false;
var bool_spaces; if (argument_count > 2) bool_spaces = argument[2]; else bool_spaces = false;

// argument 0 (arr) -- an array containing a symbol in each of its cells (as strings) to be converted into a string.
// optional argument 1 (bool_deformat) -- boolean indicating whether the string should be deformatted before being returned. defaults to false if no argument is passed
// optional argument 2 (bool_space) -- boolean indicating whether symbols should be separated by whitespace in the output string. defaults to false if no argument is passed
// converts a 1d array (where cells each contain a string denoting one symbol) to a string. returns the string.


var str = "";
for(i=0;i<array_length_1d(arr);i+=1)
{
    var symb = arr[i];
    if(bool_deformat)
    {
        symb = grmr_deformat(symb,true,true,true);
        if(bool_spaces)
        {
            symb += " ";
        };
    }
    str += symb;
}
return str;
