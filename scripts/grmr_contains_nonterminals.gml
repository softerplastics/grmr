/// grmr_contains_nonterminals(grammar)

// argument 0 (grammar) -- instance ID of the grammar in which non-terminal symbols are defined
// scans the current_string variable of the calling instance for non-terminal symbols as defined in the current grammar. returns true if one or more non-terminals are found or false if not.

var grammar = argument0;
var i = 0;
var test = false;
var size = ds_list_size((grammar).list_nonterminals);
for(i = 0; i < size; i += 1)
{
    var symb = (grammar).list_nonterminals[|i];
    if (string_pos(symb,current_string) > 0)
    {
        test = true;
    };
};
return test;
