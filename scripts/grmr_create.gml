/// grmr_create(axiom, label_string)
// creates a grammar with a single starting symbol that will run for no more than max_iter steps. 

var axiom = argument0, label_string = argument1;
// argument 0 (axiom) -- the starting symbol for a grammar, as a string. this should start and end with pipes eg. |S|
// argument 1 (label_string) -- the grammar's name, as a string. also used for the filename if the grammar is exported.
// creates a grammar and returns its ID.


var grammar = instance_create(0,0,obj_grammar);
with (grammar)
{
    starting_string += axiom;
    label = label_string;
    ds_list_add(list_nonterminals, axiom);
};

return grammar;
