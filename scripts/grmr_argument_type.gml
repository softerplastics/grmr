/// grmr_argument_type(arg)
var arg = argument0;

if (is_string(arg))
{
    return "stringdatatype"
}
else if (instance_exists(arg))
{
    if (((arg).object_index == obj_string) || ((arg).object_index == obj_slow_string))
    {
        return "stringinstance"
    }
    else
    {
        return "otherinstance"
    }
}
else
{
    return "other"
};
