/// grmr_read_safe(filepath)
var filepath = argument0;

// argument 0 (filepath) -- string indicating the location of the .grmr file to be imported.
// imports a grammar from a .grmr file, creates obj_grammar and obj_rule instance/s, rebuilding the datastructures for these instances from scratch. return the ID of the obj_grammar instance.


var list_blank = ds_list_write(ds_list_create());

ini_open(filepath)
var label = ini_read_string("head", "label", "default");
var starting_string = ini_read_string("head", "starting_string", "|S|");
var grammar = grmr_create(starting_string,label);
var num_rules = ini_read_real("head","num_rules", 0);
with(grammar)
{
    for(i=1;i<(num_rules + 1);i+=1)
    {
        var left = ini_read_string(string(i), "leftside", "default");
        var right = ini_read_string(string(i), "rightside", "default");
        var weight = ini_read_string(string(i), "prob_weight", 1);
        var rule = grmr_set_rule(left,right,weight);
    }
}
ini_close();

return grammar;
