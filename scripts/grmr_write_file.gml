/// grmr_write_file(grammar)
var grammar = argument0;

// argument 0 (grammar) -- instance ID of the grammar to be exported.
// packages a grammar into a .grmr file, saves the file to disk and returns the location of the file.

with (grammar)
{
    var varlabel = label;
    var location = working_directory + label + ".grmr";
    ini_open(location)
    ini_write_string("head", "label", label);
    ini_write_string("head", "starting_string", starting_string);
    ini_write_string("head", "list_terminals", ds_list_write(list_terminals));
    ini_write_string("head", "list_nonterminals", ds_list_write(list_nonterminals));
    ini_write_real("head","num_rules", ds_list_size(list_rules));
    for(i=0;i<ds_list_size(list_rules);i+=1)
    {
        var rule = list_rules[|i];
        with(rule)
        {
            ini_write_string(string(number), "leftside", leftside);
            ini_write_string(string(number), "rightside", rightside);
            ini_write_string(string(number), "prob_weight", prob_weight);
        }
    }
    ini_close();
}
return location;
