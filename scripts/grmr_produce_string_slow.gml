/// grmr_produce_string_slow(grammar, max_iters, bool_weighted)
var grammar = argument0, max_iters = argument1, bool_weighted = argument2;

//argument 0 (grammar) -- instance ID of the grammar with which the string will be produced
//argument 1 (max_iters) -- real number of the maximum number of iterations over which the string can be built. set to -1 to run indefinitely.
//argument 2 (bool_weighted) -- boolean indicating whether to use weighted rule selection.
//produces a string contained in an instance of obj_utter_slow, using slow production (ie. production continues at a rate of 1 iteration/frame after the function is returned). Returns the id of the obj_utter_slow instance.

var str = instance_create(0,0,obj_utter_slow);
with (str)
{
    from = grammar;
    current_string += (grammar).starting_string;
    history_string += (grammar).starting_string;
    max_iterations = max_iters;
    weighted = bool_weighted;
}

return str;
